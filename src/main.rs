extern crate gotham;
#[macro_use]
extern crate gotham_derive;
extern crate hyper;
extern crate mime;
extern crate serde;
#[macro_use]
extern crate serde_derive;
extern crate uuid;

use gotham::helpers::http::response::create_response;
use gotham::router::builder::build_simple_router;
use gotham::router::builder::*;
use gotham::router::Router;
use gotham::state::{FromState, State};
use hyper::{Body, Response, StatusCode};
use uuid::Uuid;
use gotham::handler::IntoResponse;

#[derive(Serialize)]
struct Greeting {
    id: String,
    greeting: String
}

#[derive(Deserialize, StateData, StaticResponseExtender)]
struct PathExtractor {
    name: String,
}

impl IntoResponse for Greeting {
    fn into_response(self, state: &State) -> Response<Body> {
        create_response(
            state,
            StatusCode::OK,
            mime::APPLICATION_JSON,
            serde_json::to_string(&self).expect("serialized greeting"),
        )
    }
}

fn greeting_handler(state: State) -> (State, Greeting) {
    let extractor = PathExtractor::borrow_from(&state);

    let greeting = Greeting{
        id: Uuid::new_v4().to_string(),
        greeting: format!("Hello, {}", extractor.name)
    };
    (state, greeting)
}

fn router() -> Router {
    build_simple_router(|route| {
        route.get("/greet/:name")
            .with_path_extractor::<PathExtractor>()
            .to(greeting_handler);
    })
}

/// Start a server and use a `Router` to dispatch requests
pub fn main() {
    let addr = "0.0.0.0:7878";
    println!("Listening for requests at http://{}", addr);
    gotham::start(addr, router())
}
