FROM rust:latest as BUILDER

RUN apt update && apt install musl-tools musl-dev -y

RUN rustup target add x86_64-unknown-linux-musl

WORKDIR /usr/src/app
COPY Cargo.* ./
COPY src src

RUN cargo build --release --target x86_64-unknown-linux-musl

FROM alpine:latest as CERTS
RUN apk update && apk add --no-cache ca-certificates

FROM scratch
COPY --from=BUILDER /usr/src/app/target/x86_64-unknown-linux-musl/release/rust-restful-service-skeleton /bin/
COPY --from=CERTS /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/ca-certificates.crt
ENV SSL_CERT_FILE=/etc/ssl/certs/ca-certificates.crt
ENV SSL_CERT_DIR=/etc/ssl/certs

ENTRYPOINT ["rust-restful-service-skeleton"]
